var count = 0;

$(document).ready(function () {
        if (window.sessionStorage.getItem('title') === "null") {
            window.sessionStorage.setItem('message', "Cannot find the movie you searched for.");
            window.location.href = "../html/home.html";
        }
        else {
            $('#title').html(window.sessionStorage.getItem('title').toString() + " <span id=\"year\"></span>");
        }
        if (window.sessionStorage.getItem('year') === "null") {
            $('#year').html("Unkown Year");
        }
        else {
            $('#year').html("(" + window.sessionStorage.getItem('year') + ")");
        }
        if (window.sessionStorage.getItem('rated') === "null") {
            $('#rating').html("Unknown Rating");
        }
        else {
            $('#rating').html(window.sessionStorage.getItem('rated'));
        }
        if (window.sessionStorage.getItem('released') === "null") {
            $('#releaseDate').html("Unknown Release Date");
        }
        else {
            $('#releaseDate').html(window.sessionStorage.getItem('released'));
        }
        if (window.sessionStorage.getItem('runtime') === "null") {
            $('#runtime').html("Unknown Runtime");
        }
        else {
            $('#runtime').html(window.sessionStorage.getItem('runtime'));
        }
        if (window.sessionStorage.getItem('genre') === "null") {
            $('#genre').html("Unknown Drama");
        }
        else {
            $('#genre').html(window.sessionStorage.getItem('genre'));
        }
        if (window.sessionStorage.getItem('director') === "null") {
            $('#director').html("Unknown Director");
        }
        else {
            $('#director').html(window.sessionStorage.getItem('director'));
        }
        if (window.sessionStorage.getItem('writer') === "null") {
            $('#writer').html("Unknown Writers");
        }
        else {
            $('#writer').html(window.sessionStorage.getItem('writer'));
        }
        if (window.sessionStorage.getItem('actors') === "null") {
            $('#actors').html("Unknown Actors");
        }
        else {
            $('#actors').html(window.sessionStorage.getItem('actors'));
        }
        if (window.sessionStorage.getItem('plot') === "null") {
            $('#plot').html("Unknown Plot");
        }
        else {
            $('#plot').html(window.sessionStorage.getItem('plot'));
        }
        if (window.sessionStorage.getItem('language') === "null") {
            $('#language').html("N/A");
        }
        else {
            $('#language').html(window.sessionStorage.getItem('language'));
        }
        if (window.sessionStorage.getItem('country') === "null") {
            $('#country').html("N/A");
        }
        else {
            $('#country').html(window.sessionStorage.getItem('country'));
        }
        if (window.sessionStorage.getItem('awards') === "null") {
            $('#awards').html("");
        }
        else {
            $('#awards').html(window.sessionStorage.getItem('awards'));
        }
        if (window.sessionStorage.getItem('poster') === "null") {
            $('#poster').attr("src", "");
        }
        else {
            $('#poster').attr("src", window.sessionStorage.getItem('poster'));
        }
        if (window.sessionStorage.getItem('rating0source') === "null" || window.sessionStorage.getItem('rating0value') === "null") {
            $('#rating0source').html("");
            $('#rating0value').html("");
        }
        else {
            $('#rating0source').html(window.sessionStorage.getItem('rating0source') + ":");
            $('#rating0value').html(window.sessionStorage.getItem('rating0value'));
        }
        if (window.sessionStorage.getItem('rating1source') === "null" || window.sessionStorage.getItem('rating1value') === "null") {
            $('#rating1source').html("");
            $('#rating1value').html("");
        }
        else {
            $('#rating1source').html(window.sessionStorage.getItem('rating1source') + ":");
            $('#rating1value').html(window.sessionStorage.getItem('rating1value'));
        }
        if (window.sessionStorage.getItem('rating2source') === "null" || window.sessionStorage.getItem('rating2value') === "null") {
            $('#rating2source').html("");
            $('#rating2value').html("");
        }
        else {
            $('#rating2source').html(window.sessionStorage.getItem('rating2source') + ":");
            $('#rating2value').html(window.sessionStorage.getItem('rating2value'));
        }
        if (window.sessionStorage.getItem('metascore') === "null") {
            $('#metascore').html("N/A");
        }
        else {
            $('#metascore').html(window.sessionStorage.getItem('metascore'));
        }
        if (window.sessionStorage.getItem('imdbRating') === "null") {
            $('#imdbRating').html("N/A");
        }
        else {
            $('#imdbRating').html(window.sessionStorage.getItem('imdbRating'));
        }
        if (window.sessionStorage.getItem('imdbVotes') === "null") {
            $('#imdbVotesg').html("N/A");
        }
        else {
            $('#imdbVotes').html(window.sessionStorage.getItem('imdbVotes'));

        }
        if (window.sessionStorage.getItem('dvd') === "null") {
            $('#dvd').html("N/A");
        }
        else {
            $('#dvd').html(window.sessionStorage.getItem('dvd'));
        }
        if (window.sessionStorage.getItem('boxOffice') === "null") {
            $('#boxOffice').html("N/A");
        }
        else {
            $('#boxOffice').html(window.sessionStorage.getItem('boxOffice'));
        }
        if (window.sessionStorage.getItem('production') === "null") {
            $('#production').html("N/A");
        }
        else {
            $('#production').html(window.sessionStorage.getItem('production'));
        }
        if (window.sessionStorage.getItem('website') === "null") {
            $('#website').html("N/A");
            $('#website').attr('href', "");
        }
        else {
            $('#website').html(window.sessionStorage.getItem('website'));
            $('#website').attr('href', window.sessionStorage.getItem('website'));
        }
        $("#commentButton").click(function (event) {
            event.preventDefault();
            var comment = $("#comment").val();
            if (comment !== "") {
                //count makes each id unique
                $('#commentBox').append('<div class="commentBoxes">' + comment + '</div>' +
                    '<button type="button" onclick="downButton(' + count + ')" id="down' + count + '" style="float: right;" class="uk-icon-thumbs-o-down"/>' +
                    '<button type="button" onclick="upButton(' + count + ')" id="up' + count + '" style="float: right;" class="uk-icon-thumbs-o-up"/><br>');
                $("#comment").val('');
                count++;
            }
        });
    }
);

function upButton(i) {
    if ($('#up' + i).hasClass("uk-icon-thumbs-o-up")) {
        $('#up' + i).attr('class', "uk-icon-thumbs-up");
    }
    else if ($('#up' + i).hasClass("uk-icon-thumbs-up")) {
        $('#up' + i).attr('class', "uk-icon-thumbs-o-up");
    }
    if ($('#down' + i).attr('class') === "uk-icon-thumbs-down") {
        $('#down' + i).attr('class', "uk-icon-thumbs-o-down");
    }
}

function downButton(i) {
    if ($('#down' + i).hasClass("uk-icon-thumbs-o-down")) {
        $('#down' + i).attr('class', "uk-icon-thumbs-down");
    }
    else if ($('#down' + i).hasClass("uk-icon-thumbs-down")) {
        $('#down' + i).attr('class', "uk-icon-thumbs-o-down");
    }
    if ($('#up' + i).hasClass("uk-icon-thumbs-up")) {
        $('#up' + i).attr('class', "uk-icon-thumbs-o-up");
    }
}