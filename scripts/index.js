var movies = ["The Shawshank Redemption", "The Dark Knight", "Pulp Fiction", "Fight Club", "Forrest Gump",
    "The Lord of the Rings: The Return of the King", "Goodfellas", "The Matrix", "One Flew Over the Cuckoo's Nest",
    "The Godfather", "Schindler's List", "Star Wars: Episode V - The Empire Strikes Back", " The Silence of the Lambs",
    "Spirited Away", "Saving Private Ryan", "The Usual Suspects", "Se7en", "Gladiator", "The Lion King", "Back to the Future"
    , "The Departed", "Alien", "Apocalypse Now", "Raiders of the Lost Ark", "American History X", "Terminator 2",
    "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb", "Modern Times", "Django Unchained", "Braveheart",
    "WALL·E", "Citizen Kane", "Full Metal Jacket", "Scarface", "Amadeus", "Inglourious Basterds", "2001: A Space Odyssey",
    "Toy Story", "Reservoir Dogs"];
var nums = [];
while (nums.length < 12) {
    var val = Math.floor((Math.random() * (movies.length)) + 0);
    if ($.inArray(val, nums) === -1) {
        nums.push(val);
    }
}
$(document).ready(function () {
    var URL = "https://www.omdbapi.com/?i=tt3896198&apikey=d07cb21c";

    for (var i = 0; i < 12; i++) {
        var data = "t=" + movies[nums[i]].split(' ').join('_');
        $.get(URL, data, display(i), 'json');
    }
});

function display(i) {
    return function(response) {
        $('#pic' + (i+1)).attr("src", response.Poster);
    }
}