$(document).ready(function () {
    var URL = "https://www.omdbapi.com/?i=tt3896198&apikey=d07cb21c";

    $("#searchButton").click(function (event) {
        event.preventDefault(); //prevents auto submit
        var search = $("#search").val().split(' ').join('_');
        if (search !== "") {
            var data = "t=" + search;
            $.get(URL, data, write, 'json');
        }
    });
});

function write(response) {
    if (typeof response !== 'undefined') {
        if (typeof response.Title !== 'undefined') {
            window.sessionStorage.setItem('title', response.Title);
        }
        else {
            window.sessionStorage.setItem('title', null);
        }
        if (typeof response.Year !== 'undefined') {
            window.sessionStorage.setItem('year', response.Year);
        }
        else {
            window.sessionStorage.setItem('year', null);
        }
        if (typeof response.Rated !== 'undefined') {
            window.sessionStorage.setItem('rated', response.Rated);
        }
        else {
            window.sessionStorage.setItem('rated', null);
        }
        if (typeof response.Released !== 'undefined') {
            window.sessionStorage.setItem('released', response.Released);
        }
        else {
            window.sessionStorage.setItem('released', null);
        }
        if (typeof response.Runtime !== 'undefined') {
            window.sessionStorage.setItem('runtime', response.Runtime);
        }
        else {
            window.sessionStorage.setItem('runtime', null);
        }
        if (typeof response.Genre !== 'undefined') {
            window.sessionStorage.setItem('genre', response.Genre);
        }
        else {
            window.sessionStorage.setItem('genre', null);
        }
        if (typeof response.Director !== 'undefined') {
            window.sessionStorage.setItem('director', response.Director);
        }
        else {
            window.sessionStorage.setItem('director', null);
        }
        if (typeof response.Writer !== 'undefined') {
            window.sessionStorage.setItem('writer', response.Writer);
        }
        else {
            window.sessionStorage.setItem('writer', null);
        }
        if (typeof response.Actors !== 'undefined') {
            window.sessionStorage.setItem('actors', response.Actors);
        }
        else {
            window.sessionStorage.setItem('actors', null);
        }
        if (typeof response.Plot !== 'undefined') {
            window.sessionStorage.setItem('plot', response.Plot);
        }
        else {
            window.sessionStorage.setItem('plot', null);
        }
        if (typeof response.Language !== 'undefined') {
            window.sessionStorage.setItem('language', response.Language);
        }
        else {
            window.sessionStorage.setItem('language', null);
        }
        if (typeof response.Country !== 'undefined') {
            window.sessionStorage.setItem('country', response.Country);
        }
        else {
            window.sessionStorage.setItem('country', null);
        }
        if (typeof response.Awards !== 'undefined') {
            window.sessionStorage.setItem('awards', response.Awards);
        }
        else {
            window.sessionStorage.setItem('awards', null);
        }
        if (typeof response.Poster !== 'undefined') {
            window.sessionStorage.setItem('poster', response.Poster);
        }
        else {
            window.sessionStorage.setItem('poster', null);
        }
        if (typeof response.Ratings !== 'undefined') {
            if (typeof response.Ratings[0] !== 'undefined') {
                if (typeof response.Ratings[0].Source !== 'undefined') {
                    window.sessionStorage.setItem('rating0source', response.Ratings[0].Source);
                }
                else {
                    window.sessionStorage.setItem('rating0source', null);
                }
                if (typeof response.Ratings[0].Value !== 'undefined') {
                    window.sessionStorage.setItem('rating0value', response.Ratings[0].Value);
                }
                else {
                    window.sessionStorage.setItem('rating0value', null);
                }
            }
            else {
                window.sessionStorage.setItem('rating0source', null);
                window.sessionStorage.setItem('rating0value', null);
            }
            if (typeof response.Ratings[1] !== 'undefined') {
                if (typeof response.Ratings[1].Source !== 'undefined') {
                    window.sessionStorage.setItem('rating1source', response.Ratings[1].Source);
                }
                else {
                    window.sessionStorage.setItem('rating1source', null);
                }
                if (typeof response.Ratings[1].Value !== 'undefined') {
                    window.sessionStorage.setItem('rating1value', response.Ratings[1].Value);
                }
                else {
                    window.sessionStorage.setItem('rating1value', null);
                }

            }
            else {
                window.sessionStorage.setItem('rating1source', null);
                window.sessionStorage.setItem('rating1value', null);
            }
            if (typeof response.Ratings[2] !== 'undefined') {
                if (typeof response.Ratings[2].Source !== 'undefined') {
                    window.sessionStorage.setItem('rating2source', response.Ratings[2].Source);
                }
                else {
                    window.sessionStorage.setItem('rating2source', null);
                }
                if (typeof response.Ratings[2].Value !== 'undefined') {
                    window.sessionStorage.setItem('rating2value', response.Ratings[2].Value);
                }
                else {
                    window.sessionStorage.setItem('rating2value', null);
                }
            }
            else {
                window.sessionStorage.setItem('rating2source', null);
                window.sessionStorage.setItem('rating2value', null);
            }
        }
        else {
            window.sessionStorage.setItem('rating0source', null);
            window.sessionStorage.setItem('rating0value', null);
            window.sessionStorage.setItem('rating1source', null);
            window.sessionStorage.setItem('rating1value', null);
            window.sessionStorage.setItem('rating2source', null);
            window.sessionStorage.setItem('rating2value', null);
        }
        if (typeof response.Metascore !== 'undefined') {
            window.sessionStorage.setItem('metascore', response.Metascore);
        }
        else {
            window.sessionStorage.setItem('metascore', null);
        }
        if (typeof response.imdbRating !== 'undefined') {
            window.sessionStorage.setItem('imdbRating', response.imdbRating);
        }
        else {
            window.sessionStorage.setItem('imdbRating', null);
        }
        if (typeof response.imdbVotes !== 'undefined') {
            window.sessionStorage.setItem('imdbVotes', response.imdbVotes);
        }
        else {
            window.sessionStorage.setItem('imdbVotes', null);
        }
        if (typeof response.DVD !== 'undefined') {
            window.sessionStorage.setItem('dvd', response.DVD);
        }
        else {
            window.sessionStorage.setItem('dvd', null);
        }
        if (typeof response.BoxOffice !== 'undefined') {
            window.sessionStorage.setItem('boxOffice', response.BoxOffice);
        }
        else {
            window.sessionStorage.setItem('boxOffice', null);
        }
        if (typeof response.Production !== 'undefined') {
            window.sessionStorage.setItem('production', response.Production);
        }
        else {
            window.sessionStorage.setItem('production', null);
        }
        if (typeof response.Website !== 'undefined') {
            window.sessionStorage.setItem('website', response.Website);
        }
        else {
            window.sessionStorage.setItem('website', null);
        }
    }
    if (window.location.href.indexOf("home.html") > -1) {
        window.location.href = "../html/movie.html";
    }
    else if (window.location.href.indexOf("movie.html") > -1) {
        location.reload();
    }
}