IT 353: Web Development Technologies

Each group (of 2 members at max) will develop a website similar to yelp where the user can sign up, search
for items and write a review on an item.

### Dependencies:
* uikit-2.27.5
* jquery-3.3.1
